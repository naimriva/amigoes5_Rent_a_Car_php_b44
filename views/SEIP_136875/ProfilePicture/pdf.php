<?php
include_once ('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

$obj= new ProfilePicture();
 $recordSet=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($recordSet as $row) {
        $id =  $row->id;
        $Cname = $row->Cname;
        $address = $row->address;
        $Cphone = $row->Cphone;
        $startdate = $row->startdate;
        $startmonth = $row->startmonth;
        $enddate = $row->enddate;
        $endmonth = $row->endmonth;
        $time =$row->time;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='100'> $Cname </td>";
        $trs .= "<td width='100'> $address </td>";
        $trs .= "<td width='100'> $Cphone </td>";
        $trs .= "<td width='100'> $startdate </td>";
        $trs .= "<td width='100'> $startmonth </td>";
        $trs .= "<td width='100'> $enddate </td>";
        $trs .= "<td width='100'> $endmonth </td>";
        $trs .= "<td width='200'> $time </td>";
        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Customer Name</th>
                    <th align='left' >Customer Address</th>
                    <th align='left' >Customer Phone No</th>
                    <th align='left' >Starting Date</th>
                    <th align='left' >Starting Month</th>
                    <th align='left' >Ending Date</th>
                    <th align='left' >Ending Month</th>
                    <th align='left' >Time Duration</th>


              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');