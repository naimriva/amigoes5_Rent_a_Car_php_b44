<?php
require_once("../../vendor/autoload.php");

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HOME</title>
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
            color: black;
            background:white;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
            color: black;
        }

        body {
            background: white;
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #0f192a;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}


        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: black;
        }

    </style>
    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body>


<div class="container bg-1 text-center">
    <!-- required for search, block 4 of 5 start -->

    <br>

    <!-- required for search, block 4 of 5 end -->


    <div class="navbar">
        <td><a href='../../views/SEIP_136875/index.php' class='btn btn-primary'>HOME</a> </td>

        <td><a href='../../views/SEIP_136875/ProfilePicture/index.php' class='btn btn-primary'>Transports</a> </td>

        <td><a href='../../views/SEIP_136875/Gallery/index.php' class='btn btn-primary'>Gallery</a> </td>

        <td><a href="index.php" class="btn btn-primary" role="button">Contact</a></td>

        <td><a href='../../views/SEIP_136875/Admin/login.php' class='btn btn-primary'>Log-In</a> </td>





    </div>
    <br>




    <script src="../../../resource/bootstrap/js/jquery.js"></script>

    <script>
        jQuery(function($) {
            $('#message').fadeOut (1000);
            $('#message').fadeIn (1000);
            $('#message').fadeOut (1000);
            $('#message').fadeIn (1000);
            $('#message').fadeOut (1000);
            $('#message').fadeIn (1000);
            $('#message').fadeOut (1000);
        })
        $('#delete').on('click',function(){
            document.forms[1].action="deletemultiple.php";
            $('#multiple').submit();
        });



        //select all checkboxes
        $("#select_all").change(function(){  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function(){ //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function() { //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

//check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });
    </script>

    <!-- required for search, block 5 of 5 start -->
    <script>

        $(function() {
            var availableTags = [

                <?php
                echo $comma_separated_keywords;
                ?>
            ];
            // Filter function to search only from the beginning of the string
            $( "#searchID" ).autocomplete({
                source: function(request, response) {

                    var results = $.ui.autocomplete.filter(availableTags, request.term);

                    results = $.map(availableTags, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });

                    response(results.slice(0, 15));

                }
            });


            $( "#searchID" ).autocomplete({
                select: function(event, ui) {
                    $("#searchID").val(ui.item.label);
                    $("#searchForm").submit();
                }
            });


        });

    </script>
    <!-- required for search, block5 of 5 end -->

    <img src="../../resource/bootstrap/image/freedom.png" width="400" height="300"/>
    <h1>Your Purchase Is Complete...</h1>
    <h1>Thank You For Using Our Service!</h1>
    <br>
    <div id="marquee">
        <marquee behaviour="scroll" scrollamount="7" direction="left">
            <img src="../../resource/bootstrap/image/cara.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car1.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car2.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car3.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car4.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car5.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car6.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car7.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/car8.png" width="300" height="220" />
            <img src="../../resource/bootstrap/image/carb.png" width="300" height="220" />







        </marquee>
    </div>

</body>
</html>