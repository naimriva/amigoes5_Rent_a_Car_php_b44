-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2017 at 06:50 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `soft_delete`) VALUES
(1, 'amigoes5', 'amigoes5', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, 'himu akhon', 'humaun', 'No'),
(2, 'saima', 'saima', ''),
(3, 'mishir ali', 'saima', '');

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(111) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `Dname` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `name`, `model`, `person`, `price`, `image`, `soft_delete`, `Dname`, `phone`, `experience`) VALUES
(1, 'Micro', 'corolla', '8', '15000', '2012-nissan-nv-1_653.jpg', 'Yes', 'Mukta', '999999999', '5'),
(2, 'Car', '2014-honda-odyssey', '12', '12000', '2014-honda-odyssey-15_318.jpg', 'No', 'abc', '12314253', '4'),
(3, 'Micro', '2014-toyota-4runner-41_653', '8', '5000', '2014-toyota-4runner-41_653.jpg', 'No', 'Mukta', '4567', '1'),
(4, 'Car', '2014-toyota-corolla-39_653', '5', '5000', '2014-toyota-corolla-39_653.jpg', 'No', 'Saima', '345835', '2'),
(5, 'Micro', '2015-kia-sedona-18_318', '10', '15000', '2015-kia-sedona-18_318.jpg', 'No', 'Mou', '9999456', '2'),
(6, 'Ambulance', 'bed1', '8', '5000', 'bed1.JPG', 'No', 'Kashfi', '1285345', '11'),
(7, 'Micro', 'bed5', '8', '5000', 'bed5.jpg', 'No', 'Moina', '983754', '5'),
(8, 'Car', 'Toyota-im-off-8_653', '4', '5000', 'toyota-im-off-8_653.jpg', 'No', 'Kuddus', '3684326', '3'),
(9, 'Wedding Car ', '10589', '12', '20000', 'images (1).jpg', 'No', 'Moinar Maa', '864596', '1'),
(10, 'Wedding Car', '1500', '20', '50000', 'images (2).jpg', 'No', 'Moinar Bap', '5788467', '2'),
(11, 'Wedding Car', 'Blue12', '4', '40000', 'images (3).jpg', 'No', 'Abul', '894893', '9'),
(12, 'Small Bus', '12365', '8', '25000', 'images (4).jpg', 'No', 'Tom', '543568734', '8'),
(13, 'Wedding Car', 'Bluebeauty', '4', '8000', 'images (6).jpg', 'No', 'Jerry', '8987636', '7'),
(14, 'Micro', 'B1289', '12', '36000', 'images (7).jpg', 'No', 'Superman', '876358774', '6'),
(15, 'Wedding Car', 'red', '4', '98000', 'images.jpg', 'No', 'Batman', '67673566754', '12');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `Cname` varchar(111) NOT NULL,
  `address` varchar(111) NOT NULL,
  `Cphone` varchar(111) NOT NULL,
  `startdate` varchar(100) NOT NULL,
  `startmonth` varchar(111) NOT NULL,
  `enddate` varchar(111) NOT NULL,
  `endmonth` varchar(111) NOT NULL,
  `time` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `Cname`, `address`, `Cphone`, `startdate`, `startmonth`, `enddate`, `endmonth`, `time`) VALUES
(1, 'saima', 'agrabad', '233434', '', '', '', '', ''),
(2, '', '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', '', ''),
(4, 'saima', 'ghfgkj;l', '345678', '', '', '', '', ''),
(5, 'saima', 'lalkhanbazar', '3263434534', 'Dhaka', '', '', '', ''),
(6, 'moina', 'kumilla', '44444', '13', 'September', '', '', ''),
(7, 'sammu', 'hjnmdc', '77777', '1', 'January', '2', 'January', ''),
(8, 'kuddus', 'agrabad', '00000000', '8', 'April', '9', 'April', '5 AM - 11AM'),
(9, 'saima', 'gfdsd', '7890', '5', 'January', '5', 'January', '3 PM - 9 PM'),
(10, 'saima', 'gfdsd', '7890', '5', 'August', '5', 'August', '12AM - 11PM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
