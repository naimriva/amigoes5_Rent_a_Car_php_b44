<?php

require_once("../../../vendor/autoload.php");

$pictureFile = $_FILES["profilePicture"];

$_GET["profilePicture"]=$pictureFile;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_GET);

$objProfilePicture->trash();

$objProfilePicture->moveFile();

?>