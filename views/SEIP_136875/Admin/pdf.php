<?php
include_once ('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

$obj= new ProfilePicture();
 $recordSet=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($recordSet as $row) {
        $id =  $row->id;
        $name = $row->name;
        $model = $row->model;
        $person = $row->person;
        $price = $row->price;
        $Dname = $row->Dname;
        $phone = $row->phone;
        $experience = $row->experience;
        $fileName =$row->image;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='100'> $name </td>";
        $trs .= "<td width='100'> $model </td>";
        $trs .= "<td width='100'> $person </td>";
        $trs .= "<td width='100'> $price </td>";
        $trs .= "<td width='100'> $Dname </td>";
        $trs .= "<td width='100'> $phone </td>";
        $trs .= "<td width='100'> $experience </td>";

        $trs .= "<td width='200'> $fileName </td>";
        $trs .= "<td style='padding-left: 3%'><img src='../../../views/SEIP_136875/ProfilePicture/Upload/$row->image' style=\"width:64px;height:64px;\" /></td>
";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Name</th>
                    <th align='left' >Model</th>
                    <th align='left' >Capacity</th>
                    <th align='left' >Rent</th>
                    <th align='left' >DriverName</th>
                    <th align='left' >Phone</th>
                    <th align='left' >Experience</th>
                    <th align='left' >Picture</th>


              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');