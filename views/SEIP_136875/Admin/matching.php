<?php

require_once("../../../vendor/autoload.php");

$pictureFile = $_FILES["profilePicture"];

$_POST["image"]=$pictureFile;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_POST);

$objProfilePicture->store();

$objProfilePicture->moveFile();

?>