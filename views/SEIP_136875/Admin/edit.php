<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

<style>
    body{
        background: url("../../../resource/bootstrap/image/y.jpg");
    }
</style>

</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>



    <form  class="form-group f" action="update.php" method="post" enctype="multipart/form-data">

        Please Enter Name:
        <br>
        <input class="form-control" type="text" name="personName" value="<?php echo $oneData->name ?>">
        <br>
        Please Enter Model:
        <br>
        <input class="form-control" type="text" name="model" value="<?php echo $oneData->model ?>">
        <br>
        Please Enter Rent:
        <br>
        <input class="form-control" type="text" name="price" value="<?php echo $oneData->price ?>">
        <br>
        Please Enter Capacity:
        <br>
        <input class="form-control" type="text" name="person" value="<?php echo $oneData->person ?>">
        <br>
        Enter Car's Picture:
        <input type = "file" name="image" accept=".png, .jpg, .jpeg" >
        <br>
        <img src='../../../views/SEIP_136875/ProfilePicture/Upload/<?php echo $oneData->image ?>' style="width:100px;height:100px;" /><br>
        Please Enter Driver's Name:
        <br>
        <input class="form-control" type="text" name="Dname" value="<?php echo $oneData->Dname ?>">
        <br>
        Please Enter Phone Number:
        <br>
        <input class="form-control" type="text" name="phone" value="<?php echo $oneData->phone ?>">
        <br>
        Please Enter Drivers Experience:
        <br>
        <input class="form-control" type="text" name="experience" value="<?php echo $oneData->experience ?>">
        <br>


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


