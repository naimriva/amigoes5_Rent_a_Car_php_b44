<?php
namespace App\ProfilePicture;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;
use PDOException;
class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $image;
    private $person;
    private $price;
    private $model;
    private $Dname;
    private $experience;
    private $phone;
    private $Cname;
    private $address;
    private $Cphone;
    private $usename;
    private $password;
    private $startdate;
    private $startmonth;
    private $enddate;
    private $endmonth;
    private $time;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('personName',$postData)) {
            $this->name = $postData['personName'];

        }

        if(array_key_exists('profilePicture',$postData)){

                $this->image = $postData['profilePicture'];

        }

        if(array_key_exists('experience',$postData)){

            $this->experience = $postData['experience'];

        }

        if(array_key_exists('phone',$postData)){

            $this->phone = $postData['phone'];

        }

        if(array_key_exists('Dname',$postData)){

            $this->Dname = $postData['Dname'];

        }

        if(array_key_exists('price',$postData)){

            $this->price = $postData['price'];

        }

        if(array_key_exists('model',$postData)){

            $this->model = $postData['model'];

        }

        if(array_key_exists('person',$postData)){

            $this->person = $postData['person'];

        }

        if(array_key_exists('Cname',$postData)){

            $this->Cname = $postData['Cname'];

        }

        if(array_key_exists('address',$postData)){

            $this->address = $postData['address'];

        }

        if(array_key_exists('Cphone',$postData)){

            $this->Cphone = $postData['Cphone'];

        }

        if(array_key_exists('username',$postData)){

            $this->usename = $postData['username'];

        }

        if(array_key_exists('password',$postData)){

            $this->password = $postData['password'];

        }

        if(array_key_exists('startdate',$postData)){

            $this->startdate = $postData['startdate'];

        }

        if(array_key_exists('startmonth',$postData)){

            $this->startmonth = $postData['startmonth'];

        }

        if(array_key_exists('enddate',$postData)){

            $this->enddate = $postData['enddate'];

        }

        if(array_key_exists('endmonth',$postData)){

            $this->endmonth = $postData['endmonth'];

        }

        if(array_key_exists('time',$postData)){

            $this->time = $postData['time'];

        }

    }

    public function moveFile() {
        move_uploaded_file($this->name, "C:\xampp\htdocs\Amigoes 5 Rent A Car\views\SEIP_136875\ProfilePicture\Upload".$this->image);
    }

    /**
     *
     */
    public function store() {

        $arrData = array($this->name,$this->model,$this->person,$this->price,$this->image,$this->Dname,$this->phone,$this->experience);

        $sql = "INSERT INTO car (name,model,person,price,image,Dname,phone,experience) VALUES (?,?,?,?,?,?,?,?)";

        $statement = $this->DBH->prepare($sql);

        $result = $statement->execute($arrData);

        if ($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('index.php');


    }

    public function Cstore() {

        $arrData = array($this->Cname,$this->address,$this->Cphone,$this->startdate,$this->startmonth,$this->enddate,$this->endmonth,$this->time);

        $sql = "INSERT INTO customer (Cname,address,Cphone,startdate,startmonth,enddate,endmonth,time) VALUES (?,?,?,?,?,?,?,?)";

        $statement = $this->DBH->prepare($sql);

        $result = $statement->execute($arrData);

        if ($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('../../../views/SEIP_136875/Order_Success.php');


    }

  /*  public function astore() {

        if(isset($this["username"])&&isset($this["password"])){

            $name=$_POST["username"];
            $password=$_POST["password"];

            if($name!=null && $password!=null){
                $result=mysql_query("select password from admin where username='$name' ");
                $i=0;
                if(mysql_num_rows($result)>0)
                {

                    while($row=mysql_fetch_array($result))
                    {

                        $response[$i] = $row["password"];
                        if($response[$i]==$password)
                        {
                            Utility::redirect('../../../views/SEIP_136875/Admin/index.php');
                        }
                        else
                            echo "<br/>logged failed Again Registration";

                        $i++;
        }

        Utility::redirect('../../../views/SEIP_136875/Admin/index.php');
    }}}}*/

    public function index(){

        $sql = "select * from car where soft_delete='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();


    }


    public function view(){

        $sql = "select * from car where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();



    }


    public function trashed(){

        $sql = "select * from car where soft_delete='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function update(){


        $arrData = array($this->name,$this->model,$this->price,$this->person,$this->image,$this->Dname,$this->phone,$this->experience);

        $sql = "UPDATE  car SET name=?,model=?,price=?,person=?,image=?,Dname=?,phone=?,experience=? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);


        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');


    }



    public function trash(){

        $sql = "UPDATE car SET soft_delete='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function recover(){

        $sql = "UPDATE  car SET soft_delete='No' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php');


    }




    public function delete(){

        $sql = "Delete from car  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from car  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from car  WHERE soft_delete = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from car  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from car  WHERE soft_delete = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }






    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  car SET soft_delete='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  car SET soft_delete='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from car  WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from car  WHERE id=".$id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byCapacity']) && isset($requestArray['byPrice']))  $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `person` LIKE '%".$requestArray['search']."%' OR `price` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byCapacity']) ) $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byCapacity']) )  $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `person` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['byName']) && !isset($requestArray['byPrice']) ) $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byPrice']) )  $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `price` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['byPrice']) && !isset($requestArray['byCapacity']) ) $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `price` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byPrice']) && isset($requestArray['byCapacity']) )  $sql = "SELECT * FROM `car` WHERE `soft_delete` ='No' AND `person` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->image);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->image);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords





}