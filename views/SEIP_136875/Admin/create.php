<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">

    <style>
    body{
    background: url("../../../resource/bootstrap/image/y.jpg");
    }
    </style>

</head>
<body>


<div class="container">
    <h1 style="color: #442a8d;">Car Information Entry</h1>
<form  class="form-group f" action = "store.php" method = "post" enctype="multipart/form-data">
    Please Enter Transport Type:
    <br>
    <input  class="form-control"type="text" name="name">
    <br>
    Please Enter Model:
    <br>
    <input  class="form-control"type="text" name="model">
    <br>
    Please Enter Seat Capability:
    <br>
    <input  class="form-control"type="text" name="person">
    <br>
    Please Enter Rent:
    <br>
    <input  class="form-control"type="text" name="price">
    <br>
    Please Enter Picture:
    <input type = "file" name="image" accept=".png, .jpg, .jpeg" >
    <br>
    Please Enter Driver's Name:
    <br>
    <input  class="form-control"type="text" name="Dname">
    <br>
    Please Enter Driver's Phone Number:
    <br>
    <input  class="form-control"type="text" name="phone">
    <br>
    Please Enter Driver's Experience:
    <br>
    <input  class="form-control"type="text" name="experience">
    <br>
    <br>
    <input type="submit">
    <br>
</form>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>