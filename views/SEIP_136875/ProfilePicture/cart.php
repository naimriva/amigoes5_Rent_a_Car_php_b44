<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();


?>



<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
    <title>Single Car Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" media="all" href="../../../resource/bootstrap/js/jsDatePick.css" />
    <script type="text/javascript" src="../../../resource/bootstrap/js/jsDatePick.min.1.1.js"></script>
    <script type="text/javascript">
        window.onload = function(){
            new JsDatePick({
                useMode:2,
                target:"inputField"
            });
        };
    </script>

    <style>

        td{
            border: 0px;
            background: lightsalmon;
        }

        body{
            background: url("../../../resource/bootstrap/image/m.jpg");
        }

        table{
            border: 1px;
        }
        th{
            background: lightsalmon;
        }

        tr{
            height: 30px;
            background: lightsalmon;
        }
    </style>



</head>
<body>

<div class="container">
    <h1 style="text-align: center" ;"> Your Cart </h1>
    <br>
    <table class="table table-striped table-bordered" cellspacing="0px">



        <tr>

            <td>
                <form  class="form-group f" action = "store.php" method = "post" enctype="multipart/form-data">
                    Please Enter Your Name:
                    <br>
                    <input  class="form-control"type="text" name="Cname">
                    <br>
                    Please Enter Your Address:
                    <br>
                    <input  class="form-control"type="text" name="address">
                    <br>
                    Please Enter Your Contact Number:
                    <br>
                    <input  class="form-control"type="text" name="Cphone">
                    <br>
                    Please Enter Your Starting <strong>Date / Month</strong>:
                    <br>
                    <select name="startdate" type="text" value="" style="width: 60px">

                        <option >1</option><option >2</option><option >3</option><option >4</option><option >5</option><option >6</option>
                        <option >7</option><option >8</option><option >9</option><option >10</option><option >11</option><option >12</option>
                        <option >13</option><option >14</option><option >15</option><option >16</option><option >17</option><option >18</option>
                        <option >19</option><option >20</option><option >21</option><option >22</option><option >23</option><option >24</option>
                        <option >25</option><option >26</option><option >27</option><option >28</option><option >29</option><option >30</option>
                    </select>
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >
                    <select name="startmonth" type="text" value="" style="width: 90px">
                        <option >January</option><option >February</option><option >March</option><option >April</option>
                        <option >May</option><option >Jun</option><option >July</option><option >August</option>
                        <option >September</option><option >October</option><option >November</option><option >December</option>
                    </select>
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >
                    <br><br>
                    Please Enter Your Ending <strong>Date / Month</strong>:
                    <br>
                    <select name="enddate" type="text" value="" style="width: 60px">

                        <option >1</option><option >2</option><option >3</option><option >4</option><option >5</option><option >6</option>
                        <option >7</option><option >8</option><option >9</option><option >10</option><option >11</option><option >12</option>
                        <option >13</option><option >14</option><option >15</option><option >16</option><option >17</option><option >18</option>
                        <option >19</option><option >20</option><option >21</option><option >22</option><option >23</option><option >24</option>
                        <option >25</option><option >26</option><option >27</option><option >28</option><option >29</option><option >30</option>
                    </select>
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >
                    <select name="endmonth" type="text" value="" style="width: 90px">
                        <option >January</option><option >February</option><option >March</option><option >April</option>
                        <option >May</option><option >Jun</option><option >July</option><option >August</option>
                        <option >September</option><option >October</option><option >November</option><option >December</option>
                    </select>
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >
                    <br><br>
                    Please Enter Your Time:
                    <select name="time" type="text" value="" style="width: 120px">
                        <option >3 PM - 9 PM</option>
                        <option >5 AM - 11PM</option><option >5 AM - 11AM</option><option>8 AM - 11PM</option><option>8 AM - 11AM</option>
                        <option>12PM - 1 AM</option><option>12AM - 5 AM</option><option>12PM - 6 PM</option>
                        <option>12AM - 11PM</option>
                    </select>
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

                    <br>
                    <br>
					 <strong>Please Advance Bkash payment 1000 tk for confirm booking</strong>
					 <br>
                    <input type="submit">
                    <br>
                </form>
            </td>
            <th>
                <h4>Rent Details</h4><br>
                <br>Driver Name: <h4><?= $oneData->Dname; ?></h4>
                <br><br>Contact No: <h4> <?= $oneData->phone; ?></h4>
                <br><br>Car Model: <h4> <?= $oneData->model; ?></h4>
                <br><br>Rent Per Day: <h4> <?= $oneData->price; ?></h4>


            </th>

        </tr>

        <tr>

        </tr>
        <tr>
            <td colspan="3" class="text-center"><a href='index.php' class='btn btn-info'>Back To Car List</a>

        </tr>



    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>